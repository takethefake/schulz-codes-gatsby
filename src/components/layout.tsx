import { graphql, StaticQuery } from 'gatsby'
import * as React from 'react'

import { css, Global } from '@emotion/core'

import { mq } from '../styles/media'
import { Sidebar } from './sidebar'

const Layout: React.FunctionComponent = ({ children }) => (
  <div
    css={mq({
      display: 'flex',
      flexDirection: ['column', 'row'],
      width: '100%',
    })}
  >
    <Global
      styles={css`
        a {
          color: #146580;
          text-decoration: none;
        }
        body,
        html {
          width: 100%;
          min-height: 100%;
          display: flex;
          margin: 0;
          box-sizing: border-box;
          font-family: 'lato', 'open-sans';
        }
        h1 {
          font-size: 32px;
        }
        h2 {
          font-size: 26px;
        }
        .gatsby-highlight > pre {
          max-height: 400px;
        }
        #___gatsby,
        #___gatsby > * {
          width: 100%;
          display: flex;
        }
      `}
    />
    <Sidebar />
    <div
      css={{
        display: 'flex',
        flex: '1',
        justifyContent: 'center',
      }}
    >
      <div
        css={mq({
          fontFamily:
            'medium-content-serif-font,Georgia,Cambria,"Times New Roman",Times,serif',
          letterSpacing: '.01rem',
          fontWeight: '400',
          fontStyle: 'normal',
          fontSize: '18px',
          lineHeight: '1.58',
          letterSpacing: '-.003em',
          maxWidth: '40rem',
          padding: '0px 1rem 1.45rem',
          paddingTop: 0,
          width: 'calc(100% - 1rem)',
        })}
      >
        {children}
      </div>
    </div>
  </div>
)

export default Layout
