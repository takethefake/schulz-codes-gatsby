import styled from '@emotion/styled'
import * as React from 'react'
import {
  FaEnvelope,
  FaGitlab,
  FaMediumM,
  FaPhone,
  FaStackOverflow,
  FaTwitter,
  FaXing,
  // tslint:disable-next-line
} from 'react-icons/fa'
import { mq } from '../styles/media'

const IconLink = styled.a`
  color: #000000;
  &:hover {
    color: #162c3f;
  }
`

const SocialLink: React.FunctionComponent = () => (
  <div
    css={mq({
      borderRadius: '5px',
      display: 'flex',
      justifyContent: 'space-evenly',
      padding: [0, '1rem 0'],
    })}
  >
    <IconLink href="mailto:daniel@schulz.codes">
      <FaEnvelope size={18} />
    </IconLink>
    <IconLink href="https://twitter.com/takethefake">
      <FaTwitter size={18} />
    </IconLink>
    <IconLink href="https://medium.com/@daniel_schulz">
      <FaMediumM size={18} />
    </IconLink>
    <IconLink href="https://gitlab.com/takethefake">
      <FaGitlab size={18} />
    </IconLink>
    <IconLink href="https://www.xing.com/profile/Daniel_Schulz153">
      <FaXing size={18} />
    </IconLink>
    <IconLink href="https://stackoverflow.com/users/6177879/takethefake">
      <FaStackOverflow size={18} />
    </IconLink>
  </div>
)

export { SocialLink }
