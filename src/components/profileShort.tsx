import * as React from 'react'

import { graphql, StaticQuery } from 'gatsby'
import { mq } from '../styles/media'

interface ProfileProps {
  divRef: React.RefObject<HTMLDivElement>
}

const ProfileShort: React.FunctionComponent<ProfileProps> = ({ divRef }) => (
  <StaticQuery
    query={graphql`
      query AuthorInfoQuery {
        site {
          siteMetadata {
            author {
              firstname
              lastname
              social {
                twitter
              }
              work {
                position
                name
                location
              }
              tshape
            }
          }
        }
      }
    `}
    render={({
      site: {
        siteMetadata: { author },
      },
    }) => {
      return (
        <>
          <div
            ref={divRef}
            css={mq({
              background: 'rgba(255, 255, 255, 0.4)',
              margin: ['1rem 0', '0'],
              padding: '0.5rem',
            })}
          >
            Daniel Schulz
            <br /> <br /> React Testing Dev Ops <br />
            <br /> SeniorDeveloper @ Incloud in Darmstadt
          </div>
        </>
      )
    }}
  />
)

export { ProfileShort }
