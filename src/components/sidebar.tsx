import { graphql, Link, StaticQuery } from 'gatsby'
import * as React from 'react'

import { mq } from '../styles/media'
import { Image } from './image'
import { ProfileShort } from './profileShort'
import { SocialLink } from './social'

class Sidebar extends React.Component {
  state = {
    divRefHeight: 0,
    headerFillHeight: 421,
    horizontalFixed: false,
    imageBorderRadiusTranslate: 0,
    imageFillHeight: 175,
    imageSizeTransform: 300,
    scrollTop: 0,
  }
  isScrolling
  fixedRef = React.createRef<HTMLDivElement>()
  divRef = React.createRef<HTMLDivElement>()
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
    setTimeout(() => {
      this.handleScroll(true)
    }, 100)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }
  handleScroll = (timeoutCall = false) => {
    const scrollTop = window.scrollY
    const divRefHeight =
      this.state.divRefHeight ||
      (this.divRef.current && this.divRef.current.clientHeight)

    const itemTranslate = Math.max(200 + divRefHeight - scrollTop, 60)
    const imageFillHeight = this.headerTextBelowImage()
      ? this.imageAlignedTitleHorizontal()
        ? itemTranslate - 50
        : itemTranslate + 26
      : 175 - scrollTop + divRefHeight + 50
    const borderRadiusTranslate = Math.max(0, scrollTop - divRefHeight)
    const headerFillHeight = Math.min(
      420,
      scrollTop + this.fixedRef.current.clientHeight + divRefHeight + 80
    )

    const horizontalFixed = !!Math.max(0, headerFillHeight - scrollTop)
    window.clearTimeout(this.isScrolling)
    this.isScrolling = timeoutCall
      ? setTimeout(() => {
          this.handleScroll(!timeoutCall)
        }, 100)
      : null

    this.setState({
      divRefHeight,
      headerFillHeight,
      horizontalFixed,
      imageBorderRadiusTranslate: borderRadiusTranslate,
      imageFillHeight,
      imageSizeTransform: itemTranslate,
      scrollTop,
    })
  }

  imageAlignedTitleHorizontal = () => this.state.scrollTop > 280

  headerTextBelowImage = () => this.state.scrollTop > this.state.divRefHeight

  render() {
    return (
      <StaticQuery
        query={graphql`
          query SiteTitleQuery {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={data => (
          <>
            <div
              css={mq({
                display: ['block', 'none'],
                height: [this.state.headerFillHeight, '0px'],
                width: '100%',
              })}
            />
            <div
              css={mq({
                background: '#006063',
                position: ['fixed', 'initial'],
                width: ['100%', 'auto'],
              })}
            >
              <div
                css={mq({
                  height: this.state.horizontalFixed
                    ? this.fixedRef.current
                      ? this.fixedRef.current.clientHeight
                      : 'auto'
                    : 'auto',
                  margin: '0 auto',
                  padding: '0.45rem 5rem',
                  width: [
                    this.fixedRef.current
                      ? this.fixedRef.current.clientWidth
                      : 'auto',
                    '11rem',
                  ],
                })}
              >
                <div css={{ display: 'flex' }}>
                  <div
                    ref={this.fixedRef}
                    css={mq({
                      alignItems: ['flex-end', 'center'],
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'center',
                      position: ['initial', 'fixed'],
                    })}
                  >
                    <h1 css={{ margin: 0 }}>
                      <Link
                        to="/"
                        css={{
                          color: 'white',
                          textDecoration: 'none',
                        }}
                      >
                        {data.site.siteMetadata.title}
                      </Link>
                    </h1>
                    <div
                      css={mq({
                        alignItems: ' flex-end',
                        display: ['flex', 'none'],
                        height: this.state.imageFillHeight,
                      })}
                    >
                      {
                        <div
                          css={mq({
                            display: [
                              this.headerTextBelowImage() ? 'none' : 'block',
                              'block',
                            ],
                            width: '175px',
                          })}
                        >
                          <ProfileShort divRef={this.divRef} />
                        </div>
                      }
                    </div>
                    <div
                      css={mq({
                        height: this.imageAlignedTitleHorizontal()
                          ? 'inherit'
                          : this.state.imageSizeTransform,
                        marginLeft: [
                          this.imageAlignedTitleHorizontal() ? '60px' : '0px',
                          '0px',
                        ],
                        marginTop: [
                          this.imageAlignedTitleHorizontal() ? '-50px' : '0px',
                          '0px',
                        ],
                        maxHeight: '175px',
                        maxWidth: '175px',
                        position: ['absolute', 'initial'],
                        top: ['65px', '0px'],
                        transition: 'margin-left 300ms, margin-top 300ms',
                        width: this.state.imageSizeTransform,
                      })}
                    >
                      <Image
                        imgStyle={{
                          borderRadius: `${
                            this.state.imageBorderRadiusTranslate
                          }%`,
                        }}
                      />
                    </div>
                    <div
                      css={mq({
                        width: '175px',
                      })}
                    >
                      <SocialLink />
                      <div
                        css={mq({
                          alignItems: ' flex-end',
                          display: ['none', 'block'],
                        })}
                      >
                        <ProfileShort divRef={null} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      />
    )
  }
}

export { Sidebar }
